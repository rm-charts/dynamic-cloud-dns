# Dynamic Cloud DNS Operator

Dynamic cloud dns Operator gives you a convenient way to expose your cluster with your own domain if your IP changes on a regular basis with just simple Kubernetes objects.

This is still in an early stage and not recommended for production. Also currently only AWS (Route53) is a supported cloud provider but others will follow. Follow these steps to see how easy it is to update the A or AAAA records for your domain automatically if your IP changes


## Configuration

### General

| Value       | Description   | mandatory | default |
| ----------- | ------------- | --------- | ------- |
| installCRDs | register CRDs |           | true    |


### AWS

| Value                            | Description                                                           | mandatory      | Default   |
| -------------------------------- | --------------------------------------------------------------------- | -------------- | --------- |
| auth.clients.aws.enabled         | set to true if you want to include aws as a provider and false if not | *              | true      |
| auth.clients.aws.accessKeyId     | accessKeyId of the aws account                                        | * (if enabled) | my_key    |
| auth.clients.aws.secretAccessKey | secretAccessKey of the aws account                                    | * (if enabled) | my_secret |
| auth.clients.aws.region          | the aws client needs a region even though route53 is global           |                | eu-west-1 |


## Installation

To install first add the repo

```sh
helm repo add relief-melone https://harbor.relief-melone.net/chartrepo/main
```

```sh
helm install dynamic-cloud-dns \
 -n dynamic-cloud-dns \
 relief-melone/dynamic-cloud-dns \
 --set auth.clients.aws.accessKeyId=<access-key-id> \
 --set auth.clients.aws.secretAccessKey=<secret-access-key>
```

### Separate CRD deployment

If you uninstall your release the CRD will be unregistered and so will all your CustomResources. If you want to avoid this set installCRDs to false and apply them separately from this repository

```sh
kubectl apply -f ./ci/helm/templates/crds/crd.ddns-entry.yaml
```


### Add Dynamic DNS entry

A basic dynamic dns entry would look like this

```yaml
apiVersion: dynamic-cloud-dns.io/v1
kind: DynamicDnsEntry
metadata:
  name: my-dyn-dns
spec:
  provider: aws
  ttl: 50
  reclaimPolicy: Restore
  recordSet:
    name: my.domain.net
  aws:
    hostedZoneId: some-hosted-zone-id

```
| key              | description                                                       | mandatory |
| ---------------- | ----------------------------------------------------------------- | --------- |
| provider         | the cloud provider this dns entry should be handled for           | *         |
| ttl              | time to live for the A or AAAA record                             |           |
| reclaimPolicy    | what should be done with the record after the resource is deleted | *         |
| recordSet.name   | name of the dns entry                                             | *         |
| aws.hostedZoneId | Id of the hostedZone in aws                                       | *         |

After deploying the DynamicDnsEntry to the cluser you can get information about it

```
kubectl get ddns-entries

# Output
NAMESPACE           NAME      IP             TYPE   NAME              STATE   PROVIDER   LAST_UPDATED
dynamic-cloud-dns   my-name   93.101.23.96   IPv4   my-domain.net     ok      aws        2022-01-03T01:11:11.111Z

```

#### provider
Currently only aws is supported as provider
#### reclaimPolicy
Determines what should happen to the dns record after the resource is deleted. *Restore* will restore the state before the operator interacted with the resource for the first time. *Keep* will leave everything as is *Delete* will delete the record no matter what the state was when the operator handled the resource for the first time.

#### Output

## To-Do

More cloud providers will be added. On the top of the list are currently

- MAAS (canonical MetalAsAService for internal hostnames)
- GoogleCloud
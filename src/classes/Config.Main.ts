import Config from './Config';

export default class ConfigMain extends Config {
  providers: {
    aws: {
      enabled: boolean,
      region: string
    }
  };

  constructor(env = process.env){
    super(env);

    this.providers = {
      aws: { 
        enabled: !!this.env('AWS_ACCESS_KEY_ID') && !!this.env('AWS_SECRET_ACCESS_KEY'),
        region: this.env('AWS_REGION', 'eu-west-1')
      }
    };
  }

}
import { ResourceMeta } from '@dot-i/k8s-operator';
import { KubernetesObject } from '@kubernetes/client-node';

export type IPType = 'IPv4' | 'IPv6' | null;

export interface IP {
  type: IPType
  value: string | null
}

export default interface DDNSEntry extends KubernetesObject {
  spec: {
    provider: 'aws'
    ttl: number
    reclaimPolicy: 'Delete' | 'Restore' | 'Keep'    
    recordSet: {
      name: string
    }
    aws?: {
      hostedZoneId: string 
    }
  }
  status: DDNSEntryStatus
}

export interface DDNSEntryStatus {
  currentIp?: IP
  initialState?: Array<InitialStateProperty>
  state: 'ok' | 'pending' | 'error'
  error: StatusError
  lastUpdated: string
}

export type StatusError = {
  type: string
  message: string
} | null;


export type InitialStateProperty = {
  ttl: number
  value: string
  name: string
  type: string  
};

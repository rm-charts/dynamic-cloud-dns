import Operator, { ResourceEventType, ResourceMeta } from '@dot-i/k8s-operator';

import logger from '@/log/logger.default';
import DDNSOperatorRecord, { IP } from './DDnsEntry';
import { EventEmitter } from 'stream';
import { Route53Client } from '@aws-sdk/client-route-53';
import configMain from '@/configs/config.main';
import updateDDNSEntry from '@/services/service.ddns.update_entry';
import registerDDNSEntry from '@/services/service.ddns.register_entry';
import unregisterDDNSEntry from '@/services/service.ddns.unregister_entry';
import setInitialState from '@/services/service.ddns.status.set_initial_state';
import setRecords from '@/services/service.ddns.set_records';
import updateIpIfNecessary from '@/services/service.ddns.update_ip_if_necessary';
import handleDeletion from '@/services/service.ddns.handle_deletion';
import deleteOldRecordIfNecessary from '@/services/service.ddns.delete_old_record_if_necessary';
import getFullName from '@/log/log.format.entryName';

export interface DDNSOperatorClients {
  aws: null | Route53Client;
}

export interface OperatorDDNSEntry {
  record: DDNSOperatorRecord,
  meta: ResourceMeta
}

export default class DDNSOperator extends Operator {
  currentIp: IP;
  ddnsEntries: Array<OperatorDDNSEntry>;
  emitter: EventEmitter;

  clients: DDNSOperatorClients;
  
  constructor(config = configMain){
    super(logger);

    this.currentIp = { type: null, value: null };
    this.emitter = new EventEmitter();

    this.ddnsEntries = [];

    this.clients = {
      aws: config.providers.aws.enabled 
        ? new Route53Client({
          region: config.providers.aws.region
        }) 
        : null
    };

  }

  protected async init(): Promise<void> {
    try {
      logger.info('Initializing Operator...');
      await this.checkCurrentIP();
      await this.watchResource('dynamic-cloud-dns.io', 'v1', 'ddns-entries', async (e) => {
        const obj:DDNSOperatorRecord = e.object as DDNSOperatorRecord;

        const entry:OperatorDDNSEntry = { record: obj, meta: e.meta };
      
        switch (e.type){
          case ResourceEventType.Added:
            logger.info(`${getFullName(entry)}: Resource added`);
            
            if (this.getDDNSEntry(entry.meta)){
              logger.debug(`${getFullName(entry)}: Entry has already been registered. Won't set inital state`);
              await setRecords.bind(this)(entry);

            } else {
              await setInitialState.bind(this)(entry);
              await setRecords.bind(this)(entry);
              await this.registerDDNSEntry(entry);
            }
            
            break;
          
          case ResourceEventType.Modified:
            logger.debug(`${getFullName(entry)}: Resource modified`);
            await deleteOldRecordIfNecessary.bind(this)(entry);
            await setRecords.bind(this)(entry);
            await this.updateDDNSEntry(entry);
            break;

          case ResourceEventType.Deleted:
            logger.debug(`${getFullName(entry)}: Resource deleted`);
            await handleDeletion.bind(this)(entry);
            await this.unregisterDDNSEntry(entry);
            break;
        }
      });    

      setInterval(() => {
        this.checkCurrentIP();
      }, 5000);
    } catch (err) {
      logger.error(err);
    }

    logger.info('Operator intialized');


    
  }

  private async checkCurrentIP(){
    await updateIpIfNecessary.bind(this)();
  }

  private async registerDDNSEntry(entry: OperatorDDNSEntry){
    await registerDDNSEntry.bind(this)(entry);
  }

  private async updateDDNSEntry(updatedEntry: OperatorDDNSEntry){
    await updateDDNSEntry.bind(this)(updatedEntry);
  }

  private async unregisterDDNSEntry(entry: OperatorDDNSEntry){
    await unregisterDDNSEntry.bind(this)(entry);
  }

  getDDNSEntry(meta: ResourceMeta){
    return this.ddnsEntries.find(e => 
      e.meta.namespace === meta.namespace
      && e.meta.name === meta.name
    ) || null;
  }
}
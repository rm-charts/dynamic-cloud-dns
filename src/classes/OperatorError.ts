export enum ErrorType {
  DisabledProvider = 'DisabledProvider',

  IPUnknown = 'IPUnknown',
  IPTypeUnknown = 'IPTypeUnknown',
  NetworkError = 'NetworkError',

  ProviderError_AWS = 'ProviderError_AWS',
  ProviderUnkown = 'ProviderUnkown',

  K8sError = 'K8sError',

  Other = 'Other'
}

export class OperatorError extends Error {
  type: ErrorType;
  message: string;
  details: Record<string,string>;

  constructor(type: ErrorType, message: string, details = {}){
    super();
    this.type = type;
    this.message = message;
    this.details = details;
  }
}
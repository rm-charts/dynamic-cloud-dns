import DDNSOperator from './classes/DynamicDnsOperator';
import logger from './log/logger.default';

import { dns_v2, google, dns_v1 } from 'googleapis';
import { authenticate } from '@google-cloud/local-auth';
import path from 'path';

export default async () => {
  
  const operator = new DDNSOperator();
  
  logger.info('Starting Operator...');
  await operator.start();
  logger.info('Operator started');
  // await testGoogle();
  

};

export async function testGoogle():Promise<void>{

  logger.info('Testing google apis');

  const auth = new google.auth.GoogleAuth({
    keyFile: path.join(__dirname, '../../temp/creds.json'),
    scopes: 'https://www.googleapis.com/auth/ndev.clouddns.readwrite'
  });

  google.options({ auth });

  google.dns('v2').resourceRecordSets.create({
    auth,
    managedZone: 'qualcode-net',
    location: '',
    project: 'domainpool',
    requestBody: {      
      name: 'qualcode.net',
      ttl: 500,
      type: 'A',
      signatureRrdatas: ['192.168.178.1']
    }
  });
}
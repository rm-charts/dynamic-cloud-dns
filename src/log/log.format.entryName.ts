import DDNSEntry from '@/classes/DDnsEntry';
import { OperatorDDNSEntry } from '@/classes/DynamicDnsOperator';

export default (entry:DDNSEntry | OperatorDDNSEntry):string => 
  Object.keys(entry).includes('meta')
    ? `[ ${(entry as OperatorDDNSEntry).meta.namespace}.${(entry as OperatorDDNSEntry).meta.name} ]`
    : `[ ${(entry as DDNSEntry).metadata?.namespace}.${(entry as DDNSEntry).metadata?.name} ]`;

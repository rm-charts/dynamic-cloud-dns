import DDNSOperator, { OperatorDDNSEntry } from '@/classes/DynamicDnsOperator';
import { ErrorType, OperatorError } from '@/classes/OperatorError';
import getFullName from '@/log/log.format.entryName';
import logger from '@/log/logger.default';
import { ChangeResourceRecordSetsCommand } from '@aws-sdk/client-route-53';

export const getService = () =>
  async function(this:DDNSOperator, entry:OperatorDDNSEntry){
    if (!this.clients.aws){
      const errorMessage = `${getFullName(entry)}:client for aws not enabled`;
      logger.error(errorMessage);
      return new OperatorError(ErrorType.DisabledProvider, errorMessage);
    }
    if (!entry.record.status.currentIp?.type){
      const errorMessage = `${getFullName(entry)}: current IP has no type. Cannor delete record as record type cannot be determined`;
      return new OperatorError(ErrorType.IPTypeUnknown, errorMessage);
    }

    const command = new ChangeResourceRecordSetsCommand({
      HostedZoneId: entry.record.spec.aws?.hostedZoneId,
      ChangeBatch: {
        Changes: [{
          ResourceRecordSet: {
            Name: entry.meta.name,
            Type: entry.record.status.currentIp.type,
            TTL:  entry.record.spec.ttl,
            ResourceRecords: [{ Value: entry.record.spec.recordSet.name }]
          },
          Action: 'DELETE'
        }]
      }
    });

    this.logger.info(`${getFullName(entry)}: Deleting current record of type ${entry.record.status.currentIp.type}...`);
    const deleted = await this.clients.aws.send(command);
    
    if (!deleted){
      const errorMessage = `${getFullName(entry)}: Could not delete message. Error in aws client`;
      logger.error(errorMessage);
      return new OperatorError(ErrorType.ProviderError_AWS, errorMessage);
    }

  };

export default getService();
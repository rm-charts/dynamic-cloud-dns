import DDNSOperator, { OperatorDDNSEntry } from '@/classes/DynamicDnsOperator';
import { ErrorType, OperatorError } from '@/classes/OperatorError';
import getFullName from '@/log/log.format.entryName';
import logger from '@/log/logger.default';
import deleteOldRecordAWS from './service.ddns.delete_old_record_if_necessary.aws';

export const getService = () =>
  async function(this:DDNSOperator, entry: OperatorDDNSEntry):Promise<void | OperatorError>{
    const currentEntry = this.getDDNSEntry(entry.meta);

    if (!currentEntry){
      const errorMessage = `${getFullName(entry)}: Could not determine if old entry needs to be deleted on update. Entry not found`;
      logger.error(errorMessage);
      return new OperatorError(ErrorType.Other, errorMessage);
    }
    
    let recreationNecessary = false;
    if (currentEntry.record.spec.provider !== entry.record.spec.provider){
      logger.info(`${getFullName(entry)}: Provider has changed from ${currentEntry.record.spec.provider} to ${entry.record.spec.provider}. Recreate necessary`);
      recreationNecessary = true;
    }

    if (currentEntry.record.spec.recordSet.name !== entry.record.spec.recordSet.name){
      logger.info(`${getFullName(entry)}: RecordSet name has changed from ${currentEntry.record.spec.recordSet.name} to ${entry.record.spec.recordSet.name}. Recreate necessary`);
      recreationNecessary = true;
    }

    if (recreationNecessary)
      switch (currentEntry.record.spec.provider){
        case 'aws':
          return await deleteOldRecordAWS.bind(this)(entry);
        default:
          const errorMessage = `${getFullName(entry)}: provider ${entry.record.spec.provider} unkown`;
          logger.error(errorMessage);
          return new OperatorError(ErrorType.ProviderUnkown, errorMessage);
      } else {
      logger.info(`${getFullName(entry)}: No recreation necessary. Keeping old record`);
    }

  };

export default getService();
import DDNSEntry, { InitialStateProperty } from '@/classes/DDnsEntry';
import { ErrorType, OperatorError } from '@/classes/OperatorError';
import logger from '@/log/logger.default';
import { ListResourceRecordSetsCommand, ResourceRecordSetFilterSensitiveLog, Route53Client } from '@aws-sdk/client-route-53';


export const getService = () => 
  async (client: Route53Client, entry:DDNSEntry):Promise<Array<InitialStateProperty> | OperatorError> => {
    const awsSpec = entry.spec.aws;
    if (!awsSpec)
      return new OperatorError(ErrorType.Other, 'DDNS entry did not contain sufficient specification');

    const command = new ListResourceRecordSetsCommand({
      HostedZoneId: entry.spec.aws?.hostedZoneId,
    });

    // const 

    try {
      const response = await client.send(command);
      const initialState:Array<InitialStateProperty> = [];
      if (response.$metadata.httpStatusCode !== 200)
        return new OperatorError(ErrorType.Other, 'something went wrong getting current state from aws');
      
      if (!response.ResourceRecordSets)
        return [];    
      
      const filteredRecordSets = response.ResourceRecordSets.filter(
        rs => rs.Type == 'A' || rs.Type == 'AAAA'
      );

      
      for (const rs of filteredRecordSets){
        if (rs.Type){
          initialState.push({
            name: rs.Name,
            ttl: rs.TTL,
            value: rs.ResourceRecords ? rs.ResourceRecords[0].Value : null,
            type: rs.Type
          } as InitialStateProperty);
        }
      }

      return initialState;
    
    } catch (err) {
      logger.error(err);
      return new OperatorError(ErrorType.Other, 'could not get current state from aws');
    }
  };

export default getService();
import DDNSEntry, { InitialStateProperty } from '@/classes/DDnsEntry';
import DDNSOperator from '@/classes/DynamicDnsOperator';
import { ErrorType, OperatorError } from '@/classes/OperatorError';
import getFullName from '@/log/log.format.entryName';
import logger from '@/log/logger.default';
import getInitialStateAWS from './service.ddns.get_current_state.aws';

export const getService = () => 
  async(entry:DDNSEntry, clients: DDNSOperator['clients']):Promise<Array<InitialStateProperty>| OperatorError> => {
    switch (entry.spec.provider){
      
      case 'aws':
        if (!clients.aws)
          return new OperatorError(ErrorType.DisabledProvider, 'aws has not been enalbed as a provider');
        return await getInitialStateAWS(clients.aws, entry);        
      
      default:
        const errorMessage = `${getFullName(entry)}: Cannot handle initial state. Provider ${entry.spec.provider} is not supported`;
        logger.error(errorMessage);
        return new OperatorError(ErrorType.Other, errorMessage);
    }
  };

export default getService();
  
import DDNSEntry from '@/classes/DDnsEntry';
import DDNSOperator, { OperatorDDNSEntry } from '@/classes/DynamicDnsOperator';
import { ErrorType, OperatorError } from '@/classes/OperatorError';
import getFullName from '@/log/log.format.entryName';
import logger from '@/log/logger.default';
import { ChangeResourceRecordSetsCommand, Route53, Route53Client } from '@aws-sdk/client-route-53';

export const getService = () => 
  async function(client:Route53Client, entry: DDNSEntry):Promise<void | OperatorError>{
    if (!entry.status.currentIp?.type)
      return new OperatorError(ErrorType.IPTypeUnknown, 'ip type is currently unknown');
    if (!entry.status.currentIp?.value)
      return new OperatorError(ErrorType.IPUnknown, 'ip is currently unknown');

    const command = new ChangeResourceRecordSetsCommand({
      HostedZoneId: entry.spec.aws?.hostedZoneId,
      ChangeBatch: {
        Changes: [{          
          ResourceRecordSet: {
            Name: entry.spec.recordSet.name,
            Type: entry.status.currentIp.type === 'IPv6' ? 'AAAA' : 'A',
            ResourceRecords: [{ Value: entry.status.currentIp.value }],
            TTL: entry.spec.ttl,
          },
          Action: 'DELETE'          
        }]
      }
    });

    try {
      logger.info(`${getFullName(entry)}: Deleting recordset...`);
      await client.send(command);
    } catch (err) {
      const errorMessage = `${getFullName(entry)}: Could not delete recordset`;
      logger.error(errorMessage);
      return new OperatorError(ErrorType.ProviderError_AWS, errorMessage);
    }
  };

export default getService();
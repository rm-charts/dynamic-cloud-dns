import { InitialStateProperty } from '@/classes/DDnsEntry';
import { OperatorDDNSEntry } from '@/classes/DynamicDnsOperator';
import { ErrorType, OperatorError } from '@/classes/OperatorError';
import getFullName from '@/log/log.format.entryName';
import logger from '@/log/logger.default';
import { ChangeResourceRecordSetsCommand, Route53Client } from '@aws-sdk/client-route-53';
import getRecordType from './service.getRecordTypeFromIPType';

export const getService = () =>
  async function(client: Route53Client, entry: OperatorDDNSEntry):Promise<void | OperatorError>{
    // Change to delete entry if no initial state was present
    if (!entry.record.status.initialState){
      logger.info(`${getFullName(entry)}: No initial state for. Nothing to restore...`);
      return;
    }

    if (entry.record.status.initialState.length === 0){
      logger.info(`${getFullName(entry)}: No initial state had been set. Deleting existing record...`);
      
      if (!entry.record.status.currentIp?.type){
        const errorMessage = `${getFullName}: Cannot determine type of record to delete as there is no current type set for this record`;
        logger.error(errorMessage);
        return new OperatorError(ErrorType.IPTypeUnknown, errorMessage);
      }

      if (!entry.record.status.currentIp.value){
        const errorMessage = `${getFullName}: Cannot determine which record set to delete as no IP is registered on current entry`;
        logger.error(errorMessage);
        return new OperatorError(ErrorType.IPUnknown, errorMessage);
      }

      const recordType = getRecordType(entry.record.status.currentIp.type);

      const command = new ChangeResourceRecordSetsCommand({
        HostedZoneId: entry.record.spec.aws?.hostedZoneId,
        ChangeBatch: {
          Changes: [{
            ResourceRecordSet: {
              Name: entry.record.spec.recordSet.name,
              Type: recordType,
              TTL: entry.record.spec.ttl,
              ResourceRecords: [{ Value: entry.record.status.currentIp.value }]
            },
            Action: 'DELETE'
          }]
        }
      });

      logger.info(`${getFullName(entry)}: Deleting ${recordType} record...`);
      try {
        await client.send(command);
        logger.info(`${getFullName(entry)}: Successfully deleted ${recordType} record`);
      } catch (err) {
        const errorMessage = `${getFullName(entry)}: Could not delete ${recordType} record`;
        logger.error(errorMessage);
        return new OperatorError(ErrorType.ProviderError_AWS, errorMessage);
      }


    } else 

      for (const entryToRestore of entry.record.status.initialState){
        if (entryToRestore){
          const command = new ChangeResourceRecordSetsCommand({
            HostedZoneId: entry.record.spec.aws?.hostedZoneId,
            ChangeBatch: {
              Changes: [{
                ResourceRecordSet: {
                  Name: entryToRestore.name,
                  Type: entryToRestore.type,
                  TTL:  entryToRestore.ttl,
                  ResourceRecords: [{ Value: entryToRestore.value }]
                },
                Action: 'UPSERT'
              }]
            }
          });

          logger.info(`${getFullName(entry)}: Restoring ${entryToRestore.type} record...`);
          try {
            await client.send(command);
            logger.info(`${getFullName(entry)}: Successfully restored ${entryToRestore.type} record`);
          } catch (err) {
            const errorMessage = `${getFullName(entry)}: Could not restore ${entryToRestore.type} record`;
            logger.error(errorMessage);
            return new OperatorError(ErrorType.ProviderError_AWS, errorMessage);
          }
 
        }  
      }
    
  };

export default getService();
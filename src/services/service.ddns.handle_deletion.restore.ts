import DDNSOperator, { OperatorDDNSEntry } from '@/classes/DynamicDnsOperator';
import { ErrorType, OperatorError } from '@/classes/OperatorError';
import getFullName from '@/log/log.format.entryName';
import logger from '@/log/logger.default';
import deleteOlfRecordIfNecessary from './service.ddns.delete_old_record_if_necessary';
import restoreRecordAWS from './service.ddns.handle_deletion.restore.aws';


export const getService = () => 
  async function(this:DDNSOperator, entry: OperatorDDNSEntry):Promise<void | OperatorError> {
    switch (entry.record.spec.provider){
      case 'aws':
        if (!this.clients.aws){
          const errorMessage = `${getFullName(entry)}: Could not restore Record as aws client is not available`;
          logger.error(errorMessage);
          return new OperatorError(ErrorType.ProviderError_AWS, errorMessage);
        }
        return await restoreRecordAWS(this.clients.aws, entry);
      default:
        const errorMessage = `${getFullName(entry)}: ${entry.record.spec.provider} does not yet support record deletion`;
        logger.error(errorMessage);
        return new OperatorError(ErrorType.Other, errorMessage);
    }
  };

export default getService();
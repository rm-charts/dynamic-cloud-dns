import DDNSOperator, { OperatorDDNSEntry } from '@/classes/DynamicDnsOperator';
import { OperatorError } from '@/classes/OperatorError';
import getFullName from '@/log/log.format.entryName';
import logger from '@/log/logger.default';
import deleteRecord from './service.ddns.handle_deletion.delete';
import restoreRecord from './service.ddns.handle_deletion.restore';
import handleError from './service.ddns.set_error_state';

export const getService = () =>
  async function(this: DDNSOperator, entry: OperatorDDNSEntry){
    switch (entry.record.spec.reclaimPolicy){
      case 'Delete':
        logger.info(`${getFullName(entry)}: Deleting Record...`);
        const deletionResult = await deleteRecord.bind(this)(entry);
        if (deletionResult instanceof OperatorError){
          logger;
          await handleError.bind(this)(deletionResult, entry);
        } else {
          logger.info(`${getFullName(entry)}: Successfully deleted Record`);
        }
        break;          
      case 'Keep':
        break;
      case 'Restore':
        logger.info(`${getFullName(entry)}: Restoring Record...`);
        const restoreResult = await restoreRecord.bind(this)(entry);
        if (restoreResult instanceof OperatorError){
          await handleError.bind(this)(restoreResult, entry);
        } else {
          logger.info(`${getFullName(entry)}: Successfully restored Record`);
        }
        break;
    }
  };

export default getService();
import DDNSOperator from '@/classes/DynamicDnsOperator';
import { OperatorError } from '@/classes/OperatorError';
import getFullName from '@/log/log.format.entryName';
import logger from '@/log/logger.default';
import { ResourceMeta } from '@dot-i/k8s-operator';
import serviceDdnsSet_records from './service.ddns.set_records';

export const getService = () =>
  async function(this:DDNSOperator):Promise<void>{
    for (const entry of this.ddnsEntries){
      logger.info(`${getFullName(entry)}: Handeling IP change...`);
      await serviceDdnsSet_records.bind(this)(entry);
      logger.info(`${getFullName(entry)}: IP change handled`);

    }
  };

export default getService();
import DDNSEntry from '@/classes/DDnsEntry';
import DDNSOperator, { OperatorDDNSEntry } from '@/classes/DynamicDnsOperator';
import { ErrorType, OperatorError } from '@/classes/OperatorError';
import getFullName from '@/log/log.format.entryName';
import logger from '@/log/logger.default';

export const getService = () =>
  async function(
    this:DDNSOperator, status: Partial<DDNSEntry['status']> ,entry: OperatorDDNSEntry
  ):Promise<void | OperatorError>{
    const result = await this.patchResourceStatus(entry.meta, status);
    if (!result){
      const errorMessage = `${getFullName(entry)}: Could not patch state`;
      logger.error(errorMessage);
      return new OperatorError(ErrorType.K8sError, errorMessage);
    } else {
      Object.assign(entry.meta, result);
    }
  };

export default getService();
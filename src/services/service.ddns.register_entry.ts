import DDNSEntry, { DDNSEntryStatus } from '@/classes/DDnsEntry';
import DDNSOperator, { OperatorDDNSEntry } from '@/classes/DynamicDnsOperator';
import getFullName from '@/log/log.format.entryName';
import logger from '@/log/logger.default';
import { ResourceMeta } from '@dot-i/k8s-operator';

export const getService = () => 
  async function(this: DDNSOperator, newEntry: OperatorDDNSEntry){
    logger.info(`${getFullName(newEntry)}: Registering ddns entry...`);
    this.ddnsEntries.push(newEntry);
    logger.info(`${getFullName(newEntry)}: Successfully registered ddns entry. Keeping track of ${this.ddnsEntries.length} entries now`);

  };

export default getService();
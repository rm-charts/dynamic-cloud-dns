import DDNSEntry, { DDNSEntryStatus } from '@/classes/DDnsEntry';
import DDNSOperator, { OperatorDDNSEntry } from '@/classes/DynamicDnsOperator';
import { OperatorError } from '@/classes/OperatorError';
import getFullName from '@/log/log.format.entryName';
import logger from '@/log/logger.default';
import { ResourceMeta } from '@dot-i/k8s-operator';
import patchStatus from './service.ddns.patch_status_for_entry';

export const getService = () => 
  async function(this: DDNSOperator, error:OperatorError, entry: OperatorDDNSEntry){
    const status:Partial<DDNSEntryStatus> = {
      state: 'error',
      error: {
        message: error.message,
        type: error.type
      }
    };
    logger.error(`${getFullName(entry)}: Setting status to error with message: ${status.error?.message || 'N/A'}...`);
    const err = await patchStatus.bind(this)(status, entry);
    if (err){
      logger.info(`${getFullName(entry)}: Status has been set to error`);
    }
    this.patchResourceStatus(entry.meta, status);
  };

export default getService();
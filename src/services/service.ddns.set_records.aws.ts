import DDNSEntry, { IP } from '@/classes/DDnsEntry';
import DDNSOperator from '@/classes/DynamicDnsOperator';
import { ErrorType, OperatorError } from '@/classes/OperatorError';
import getFullName from '@/log/log.format.entryName';
import logger from '@/log/logger.default';
import { ChangeResourceRecordSetsCommand, Route53Client } from '@aws-sdk/client-route-53';

export const getService = () =>
  async function(this: DDNSOperator, client:Route53Client, entry:DDNSEntry):Promise<void| OperatorError>{
    if (!this.currentIp.type)
      return new OperatorError(ErrorType.IPTypeUnknown, 'ip type is currently unknown');
    if (!this.currentIp.value)
      return new OperatorError(ErrorType.IPUnknown, 'ip is currently unknown');

    const command = new ChangeResourceRecordSetsCommand({
      HostedZoneId: entry.spec.aws?.hostedZoneId,
      ChangeBatch: {
        Changes: [{
          ResourceRecordSet: {
            Name: entry.spec.recordSet.name,
            Type: this.currentIp.type === 'IPv6' ? 'AAAA' : 'A',
            ResourceRecords: [{ Value: this.currentIp.value }],
            TTL: entry.spec.ttl,
          },          
          Action: 'UPSERT'
        }],
      }
    });

    try {
      logger.info(`${getFullName(entry)}: Updating records...`);
      await client.send(command);
      logger.info(`${getFullName(entry)}: Updated records`);
    } catch (err) {
      const errorMessage = `${getFullName(entry)}: Could not set recordset`;
      logger.error(errorMessage);
      return new OperatorError(ErrorType.ProviderError_AWS, errorMessage);
    }
    
  };

export default getService();
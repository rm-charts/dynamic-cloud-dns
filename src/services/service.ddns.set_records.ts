import DDNSEntry, { DDNSEntryStatus, IP } from '@/classes/DDnsEntry';
import DDNSOperator, { DDNSOperatorClients, OperatorDDNSEntry } from '@/classes/DynamicDnsOperator';
import { ErrorType, OperatorError } from '@/classes/OperatorError';
import getFullName from '@/log/log.format.entryName';
import logger from '@/log/logger.default';
import { ResourceMeta } from '@dot-i/k8s-operator';
import setErrorState from './service.ddns.set_error_state';
import setRecordsAws from './service.ddns.set_records.aws';
import setStatusToOK from './service.ddns.status.set_ok';

export const recordsNeedsUpdating = 
function(this:DDNSOperator, entry: OperatorDDNSEntry):boolean{
  const beforeChanges = this.getDDNSEntry(entry.meta);
  if (!beforeChanges)
    return true;

  return beforeChanges.record.status.currentIp?.value !== entry.record.status.currentIp?.value
  || beforeChanges.record.spec.ttl !== entry.record.spec.ttl
  || this.currentIp.value !== entry.record.status.currentIp?.value
  || beforeChanges.record.spec.recordSet.name !== entry.record.spec.recordSet.name
  || beforeChanges.record.spec.provider !== entry.record.spec.provider;
};

export const getService = () =>
// Still needs proper implementation if provider or recordNames change
  async function (this:DDNSOperator,entry:OperatorDDNSEntry):Promise<void|OperatorError>{
    if (!recordsNeedsUpdating.bind(this)(entry)){
      logger.debug(`${getFullName(entry)}: Value is already up to date. Moving on...`);
      return;
    }

    switch (entry.record.spec.provider){      
      
      case 'aws':
        if (!this.clients.aws)
          await setErrorState
            .bind(this)(new OperatorError(ErrorType.DisabledProvider, 'aws has not been enalbed as a provider'), entry);
        else {
          const response = await setRecordsAws.bind(this)(this.clients.aws, entry.record);
          if (response instanceof OperatorError)
            await setErrorState.bind(this)(response, entry);
          else {
            await setStatusToOK.bind(this)(entry);
          }
        }
        break;
              
      default:
        return new OperatorError(ErrorType.Other, `setting records for provider ${entry.record.spec.provider} is currently not supported`);
    }
  };

export default getService();
import DDNSEntry, { DDNSEntryStatus } from '@/classes/DDnsEntry';
import DDNSOperator, { OperatorDDNSEntry } from '@/classes/DynamicDnsOperator';
import { OperatorError } from '@/classes/OperatorError';
import getFullName from '@/log/log.format.entryName';
import logger from '@/log/logger.default';
import { ResourceMeta } from '@dot-i/k8s-operator';
import getCurrentState from './service.ddns.get_current_state';
import patchStatus from './service.ddns.patch_status_for_entry';
import setErrorState from './service.ddns.set_error_state';

export const getService = () =>
  async function(this:DDNSOperator, entry:OperatorDDNSEntry){
    if (entry.record.status?.initialState && entry.record.status?.initialState?.length >= 0){
      logger.info(`${getFullName(entry)}: Initial state already present in resource. Will be left unchanged in status`);
      return entry.record.status.initialState;
    }

    const currentStateRepsonse = await getCurrentState(entry.record, this.clients);

    if (currentStateRepsonse instanceof OperatorError){
      logger.error(`${getFullName(entry)}: ${currentStateRepsonse.message}`);
      await setErrorState.bind(this)(currentStateRepsonse, entry);
    } else {
      const err = await patchStatus.bind(this)({
        initialState: currentStateRepsonse
      }, entry);
      if (err){
        logger.info(`${getFullName(entry)}: Could not set initial state`);
      } else {
        logger.info(`${getFullName(entry)}: Successfully set initial state`);
      }
    }

    return currentStateRepsonse;
  };

export default getService();

import { DDNSEntryStatus } from '@/classes/DDnsEntry';
import DDNSOperator, { OperatorDDNSEntry } from '@/classes/DynamicDnsOperator';
import { ErrorType, OperatorError } from '@/classes/OperatorError';
import getFullName from '@/log/log.format.entryName';
import logger from '@/log/logger.default';
import patchStatus from './service.ddns.patch_status_for_entry';
import serviceDdnsSet_error_state from './service.ddns.set_error_state';

export const getService = () =>
  async function(this:DDNSOperator, entry: OperatorDDNSEntry):Promise<void|OperatorError>{
    const status: DDNSEntryStatus = {
      error: null,
      state: 'ok',
      currentIp: this.currentIp,
      lastUpdated: new Date().toISOString()
    };
    logger.info(`${getFullName(entry)}: Setting status OK...`);
    try {
      const err = await patchStatus.bind(this)(status, entry);
      if (err)
        logger.error(`${getFullName(entry)}: Could not set status to OK`);
      else
        logger.info(`${getFullName(entry)}: Successfully set status set to OK`);
    } catch (err) {
      logger.error(`${getFullName(entry)}: Could not set Error state.`);
      logger.debug(JSON.stringify(err));
    }
    

  };

export default getService();
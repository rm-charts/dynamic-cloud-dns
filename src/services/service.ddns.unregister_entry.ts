import DDNSEntry from '@/classes/DDnsEntry';
import DDNSOperator, { OperatorDDNSEntry } from '@/classes/DynamicDnsOperator';
import getFullName from '@/log/log.format.entryName';
import logger from '@/log/logger.default';

export const getService = () => 
  async function(this: DDNSOperator, entry: OperatorDDNSEntry){
    logger.info(`${getFullName(entry)}: Trying to unregister DDNSEntry`);
    
    const index = this.ddnsEntries
      .findIndex( entry => 
        (entry.meta.namespace && entry.meta.name)
        && (
          entry.meta.namespace == entry.meta.namespace
          &&
          entry.meta.name == entry.meta.name
        )
      );


    if (index !== -1){      
      this.ddnsEntries.splice(index, 1);
      logger.info(`${getFullName(entry)}: successfully unregistered. Keeping track of ${this.ddnsEntries.length} entries now`);
    } else {
      logger.warn(`${getFullName(entry)}: Could not find DDNs in registered entries`);
    }
  };

export default getService();
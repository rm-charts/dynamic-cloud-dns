import DDNSEntry from '@/classes/DDnsEntry';
import DDNSOperator, { OperatorDDNSEntry } from '@/classes/DynamicDnsOperator';
import getFullName from '@/log/log.format.entryName';
import logger from '@/log/logger.default';

export const getService = () => 
  async function(this: DDNSOperator, updatedEntry: OperatorDDNSEntry){
    logger.info(`${getFullName(updatedEntry)}: Updating ddns entry...`);
    const index = this.ddnsEntries
      .findIndex( entry => 
        (entry.meta.namespace && entry.meta.name)
        && (
          entry.meta.namespace == entry.meta.namespace
          &&
          entry.meta.name == entry.meta.name
        )
      );
    if (index !== -1){
      const entry = this.ddnsEntries[index];
      Object.keys(key => delete entry[key]);
      Object.assign(entry, updatedEntry);
      logger.info(`${getFullName(entry)}: Successfully updated ddns entry ${entry.record.spec.recordSet.name}. Keeping track of ${this.ddnsEntries.length} entries now`);
    } else {
      logger.warn(`${getFullName(updatedEntry)}: Could not find ddns entry`);
    }
  };

export default getService();
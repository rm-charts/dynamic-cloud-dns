import DDNSOperator from '@/classes/DynamicDnsOperator';
import { OperatorError } from '@/classes/OperatorError';
import logger from '@/log/logger.default';
import handleIPChange from './service.ddns.handle_ip_change';
import { getCurrentIp } from './service.update_ip';

export const getService = () =>
  async function(this: DDNSOperator){
    logger.debug('Checking IP...');
    const previusIp = this.currentIp;
    const currentIp = await getCurrentIp();

    if (currentIp instanceof OperatorError){
      logger.warn('Fetching IP failed. Not updating IP...');
      return;
    }
      
    
    if (this.currentIp.value !== currentIp.value){
      this.currentIp = currentIp;
      logger.info('IP change detected');
      logger.info(`Previous IP: Type: ${previusIp.type} \t Value: ${previusIp.value}`);
      logger.info(`Current IP:  Type: ${this.currentIp.type} \t Value: ${this.currentIp.value}`);
      await handleIPChange.bind(this)();
    } else {
      logger.silly('IP unchanged');
      logger.silly(`Current IP:  Type: ${currentIp.type} \t Value: ${this.currentIp.value}`);
    }
  };

export default getService();
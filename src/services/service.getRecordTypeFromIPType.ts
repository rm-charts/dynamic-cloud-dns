import { IPType } from '@/classes/DDnsEntry';

export default (ipType: IPType):'A' | 'AAAA' => 
  ipType === 'IPv4'
    ? 'A'
    : 'AAAA';
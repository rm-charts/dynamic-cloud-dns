import DDNSOperator from '@/classes/DynamicDnsOperator';
import { ErrorType, OperatorError } from '@/classes/OperatorError';
import getFullName from '@/log/log.format.entryName';
import logger from '@/log/logger.default';
import axios from 'axios';
import { query } from 'winston';

const defaultClient = axios.create({
  baseURL: 'https://api.ipify.org',
  params: {
    format: 'json'
  }
});

export const isIPv4 =(inp:string):boolean => {
  return /^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$/.test(inp);
};

export const isIPv6 = (inp:string):boolean => {
  return /([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])/.test(inp);
};

export const getCurrentIp = async ():Promise<DDNSOperator['currentIp'] | OperatorError> => {
  try {
    const res = await defaultClient.get('/');
    const ip = res.data?.ip || null;
    
    if (isIPv4(ip))
      return { type: 'IPv4', value: ip };
    else if (isIPv6(ip)) 
      return { type: 'IPv6', value: ip };
    else {
      logger.error(`IP ${ip} could neither be recognized as IPv4 nor IPv6`);
      return { type: null, value: null };
    }
  } catch (err) {
    const errorMessage = `Could not get current IP. Calling ${defaultClient.getUri()} failed`;
    logger.error(errorMessage);
    return new OperatorError(ErrorType.NetworkError, errorMessage);
  }
};